#include <gtk/gtk.h>

struct app_widgets {
  GtkWindow *window_main;
  GtkTextView *text_view_memory;
  GtkTextBuffer *text_buffer_main;
};

int
main (int    argc,
      char **argv)
{
  GtkBuilder  *builder;
  struct app_widgets *widgets = g_slice_new (struct app_widgets);

  gtk_init (&argc, &argv);

  builder = gtk_builder_new_from_file ("ui/window_main.ui");

  widgets->window_main = GTK_WINDOW (gtk_builder_get_object (builder, "window_main"));
  widgets->text_view_memory = GTK_TEXT_VIEW (gtk_builder_get_object (builder, "text_view_main"));
  widgets->text_buffer_main = GTK_TEXT_BUFFER (gtk_builder_get_object (builder, "text_buffer_main"));

  gtk_builder_connect_signals (builder, widgets);
  
  g_object_unref (builder);

  gtk_widget_show (widgets->window_main);
  gtk_main ();

  g_slice_free (struct app_widgets, widgets);

  return 0;
}

void on_btn_get_memory_sizes_clicked (GtkButton          *button,
                                      struct app_widgets *widgets)
{
  gboolean result;
  gchar *std_out, *std_err;
  gint exit_state;
  GError *err;

  result = g_spawn_command_line_sync ("free -h",
                                      &std_out,
                                      &std_err,
                                      &exit_state,
                                      &err);

  if (!result)
    {
      g_print ("An error occurred.\n");
    }
  else
    {
      gtk_text_buffer_set_text (widgets->text_buffer_main, std_out, -1);
    }
}

void on_window_main_destroy ()
{
  gtk_main_quit ();
}
